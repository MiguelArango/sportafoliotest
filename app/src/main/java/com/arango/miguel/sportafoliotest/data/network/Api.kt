package com.arango.miguel.sportafoliotest.data.network

import com.arango.miguel.sportafoliotest.domain.model.Post
import retrofit2.Call
import retrofit2.http.*

interface Api {


    @GET("/posts/")
    fun getPosts(): Call<List<Post>>

    @GET("/posts/{postId}")
    fun getPostDetailById(@Path("postId") postId: Int): Call<Post>

}