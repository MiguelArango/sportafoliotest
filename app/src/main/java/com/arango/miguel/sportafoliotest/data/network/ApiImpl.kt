package com.arango.miguel.sportafoliotest.data.network

import com.arango.miguel.sportafoliotest.domain.model.Post
import retrofit2.Call

class ApiImpl: Api {


    override fun getPosts(): Call<List<Post>> {
        val api = ApiConnection().connectionApi().requestSyncCall()
        return api.getPosts()
    }

    override fun getPostDetailById(postId: Int): Call<Post> {
        val api = ApiConnection().connectionApi().requestSyncCall()
        return api.getPostDetailById(postId)
    }
}