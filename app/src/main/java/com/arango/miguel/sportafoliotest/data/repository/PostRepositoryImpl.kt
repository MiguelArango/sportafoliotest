package com.arango.miguel.sportafoliotest.data.repository

import com.arango.miguel.sportafoliotest.data.network.ApiImpl
import com.arango.miguel.sportafoliotest.domain.callback.GetPostDetailCallback
import com.arango.miguel.sportafoliotest.domain.callback.GetPostsCallback
import com.arango.miguel.sportafoliotest.domain.model.Post
import com.arango.miguel.sportafoliotest.domain.repository.PostRepository
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class PostRepositoryImpl: PostRepository {


    override fun getPosts(callback: GetPostsCallback) {

        val api = ApiImpl()
        val responseCall: Call<List<Post>> = api.getPosts()
        responseCall.enqueue(object : Callback<List<Post>> {
            override fun onResponse(call: Call<List<Post>>, response: Response<List<Post>>) {
                if (response.isSuccessful()) {
                    callback.onGetPostsSuccess(response.body()!!)
                } else {
                    callback.onGetPostsError("Ocurrió un error")
                }
            }

            override fun onFailure(call: Call<List<Post>>, t: Throwable) {
                callback.onGetPostsError(t.message.toString())
            }
        })
    }

    override fun getPostDetail(postId: Int, callback: GetPostDetailCallback) {

        val api = ApiImpl()
        val responseCall: Call<Post> = api.getPostDetailById(postId)
        responseCall.enqueue(object : Callback<Post> {
            override fun onResponse(call: Call<Post>, response: Response<Post>) {
                if (response.isSuccessful()) {
                    callback.onGetPostDetailSuccess(response.body()!!)
                } else {
                    callback.onGetPostDetailError("Ocurrió un error")
                }
            }

            override fun onFailure(call: Call<Post>, t: Throwable) {
                callback.onGetPostDetailError(t.message.toString())
            }
        })

    }
}