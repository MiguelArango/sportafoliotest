package com.arango.miguel.sportafoliotest.domain.callback

import com.arango.miguel.sportafoliotest.domain.model.Post

interface GetPostDetailCallback {

    fun onGetPostDetailSuccess(post: Post)
    fun onGetPostDetailError(errorMessage: String)

}