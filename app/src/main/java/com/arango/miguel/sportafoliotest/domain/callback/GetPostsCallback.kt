package com.arango.miguel.sportafoliotest.domain.callback

import com.arango.miguel.sportafoliotest.domain.model.Post

interface GetPostsCallback {

    fun onGetPostsSuccess(postList: List<Post>)
    fun onGetPostsError(errorMessage: String)
}