package com.arango.miguel.sportafoliotest.domain.interactor

import com.arango.miguel.sportafoliotest.domain.callback.GetPostsCallback
import com.arango.miguel.sportafoliotest.domain.repository.PostRepository

class MainInteractor(private val postRepository: PostRepository) {

    fun getPosts(callback: GetPostsCallback){
        postRepository.getPosts(callback)
    }
}