package com.arango.miguel.sportafoliotest.domain.interactor

import com.arango.miguel.sportafoliotest.domain.callback.GetPostDetailCallback
import com.arango.miguel.sportafoliotest.domain.repository.PostRepository

class PostDetailInteractor(private val postRepository: PostRepository) {

    fun getPostDetail(postId: Int, callback: GetPostDetailCallback){
        postRepository.getPostDetail(postId,callback)
    }
}