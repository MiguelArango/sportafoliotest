package com.arango.miguel.sportafoliotest.domain.model

import java.io.Serializable

data class Post(
    val userId: Int,
    val id: Int,
    var title: String,
    var body: String
): Serializable