package com.arango.miguel.sportafoliotest.domain.repository

import com.arango.miguel.sportafoliotest.domain.callback.GetPostDetailCallback
import com.arango.miguel.sportafoliotest.domain.callback.GetPostsCallback

interface PostRepository {

    fun getPosts(callback: GetPostsCallback)
    fun getPostDetail(postId: Int, callback: GetPostDetailCallback)
}