package com.arango.miguel.sportafoliotest.ui.activity

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.arango.miguel.sportafoliotest.R
import com.arango.miguel.sportafoliotest.ui.adapter.PostListAdapter
import com.arango.miguel.sportafoliotest.domain.model.Post
import com.arango.miguel.sportafoliotest.ui.presenter.MainPresenter
import com.arango.miguel.sportafoliotest.ui.view.MainView
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), MainView, PostListAdapter.PostListener {


    override fun onClickPost(clickedPost: Post) {
        val intent = Intent(this, PostDetailActivity::class.java)
        intent.putExtra("mode", "view")
        intent.putExtra("post", clickedPost)
        startActivity(intent)
    }

    override fun onLongClickpost(longClickedPost: Post) {
        val intent = Intent(this, PostDetailActivity::class.java)
        intent.putExtra("mode", "edit")
        intent.putExtra("post", longClickedPost)
        startActivityForResult(intent, REQUEST_CODE_EDIT_POST)
    }

    override fun showError(errorMessage: String) {
        Toast.makeText(this, errorMessage, Toast.LENGTH_LONG).show()
    }

    override fun renderPosts(postList: List<Post>) {
        this.postList.clear()
        this.postList.addAll(postList)
        postsAdapter.notifyDataSetChanged()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        when(requestCode){
            REQUEST_CODE_EDIT_POST ->
                if (resultCode == Activity.RESULT_OK) {
                    val editedPost = data?.getSerializableExtra("post") as Post
                    for(post in postList){

                        if(post.id == editedPost.id){
                            post.title = editedPost.title
                            post.body = editedPost.body
                            postsAdapter.notifyDataSetChanged()

                        }

                    }
                }

        }
    }

    val postList: ArrayList<Post> = ArrayList()
    lateinit var postsAdapter: PostListAdapter
    lateinit var presenter: MainPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initViewComponents()
        initPresenter()
    }

    fun initViewComponents(){
        postsAdapter = PostListAdapter(postList, this)

        rvPosts.apply {
            adapter = postsAdapter
            layoutManager = LinearLayoutManager(this@MainActivity)
        }
    }

    fun initPresenter(){
        presenter = MainPresenter()
        presenter.setView(this)
        presenter.initialize()
    }

    companion object{
        private const val REQUEST_CODE_EDIT_POST = 120

    }
}
