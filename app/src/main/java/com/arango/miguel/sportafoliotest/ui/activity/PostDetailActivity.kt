package com.arango.miguel.sportafoliotest.ui.activity

import android.app.Activity
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.core.view.isVisible
import com.arango.miguel.sportafoliotest.R
import com.arango.miguel.sportafoliotest.domain.model.Post
import com.arango.miguel.sportafoliotest.ui.presenter.PostDetailPresenter
import com.arango.miguel.sportafoliotest.ui.view.PostDetailView
import kotlinx.android.synthetic.main.activity_post_detail.*
import android.content.Intent



class PostDetailActivity : AppCompatActivity(), PostDetailView, View.OnClickListener {

    override fun renderPost(postDetail: Post) {
        tvTitle.setText(postDetail.title)
        tvBody.setText(postDetail.body)
    }

    override fun onClick(v: View?) {
        val itemId = v?.id
        when(itemId){
            R.id.fabEdit ->
                tryToEdit()
        }
    }

    override fun showError(errorMessage: String) {
        Toast.makeText(this, errorMessage, Toast.LENGTH_LONG).show()
    }

    fun tryToEdit(){
        tvTitle.error = null
        tvBody.error = null

        val title = tvTitle.text.toString()
        val body = tvBody.text.toString()

        var focusView: View? = null
        var cancel: Boolean = false


        if(body.isEmpty()){
            tvBody.error = getString(R.string.error_field_required)
            cancel = true
            focusView = tvBody
        }
        if(title.isEmpty()){
            tvTitle.error = getString(R.string.error_field_required)
            cancel = true
            focusView = tvTitle
        }

        if(cancel){
            focusView?.requestFocus()
        }   else{
            edit(title, body)
        }
    }

    fun edit(title: String, body: String){
        val data = Intent()
        val newPost: Post = Post(currentPost.userId, currentPost.id, title, body)
        data.putExtra("post", newPost)
        setResult(Activity.RESULT_OK, data)
        finish()

    }

    lateinit var mode: String
    lateinit var currentPost: Post
    lateinit var presenter: PostDetailPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_post_detail)

        mode = intent.getStringExtra("mode")
        currentPost = intent.getSerializableExtra("post") as Post

        initViewComponents()
        initPresenter()
    }

    fun initViewComponents(){
        if(mode.contentEquals("view")){
            fabEdit.hide()
            tvTitle.isEnabled = false
            tvBody.isEnabled = false
        }   else if(mode.contentEquals("edit")){
            fabEdit.show()
            tvTitle.isEnabled = true
            tvBody.isEnabled = true
        }

        tvTitle.setText(currentPost.title)
        tvBody.setText(currentPost.body)


        fabEdit.setOnClickListener(this)

    }

    fun initPresenter(){
        presenter = PostDetailPresenter(currentPost, mode)
        presenter.setView(this)
        presenter.initialize()

    }


}
