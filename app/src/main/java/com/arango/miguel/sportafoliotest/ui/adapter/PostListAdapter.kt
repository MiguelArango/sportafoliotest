package com.arango.miguel.sportafoliotest.ui.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.arango.miguel.sportafoliotest.R
import com.arango.miguel.sportafoliotest.domain.model.Post

class PostListAdapter(private val postList: List<Post>, private val listener: PostListener) : RecyclerView.Adapter<PostListAdapter.PostViewHolder>() {

    interface PostListener{
        fun onClickPost(clickedPost: Post)
        fun onLongClickpost(longClickedPost: Post)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PostViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return PostViewHolder(inflater, parent)
    }

    override fun onBindViewHolder(holder: PostViewHolder, position: Int) {
        holder.bind(postList[position])

        holder.itemView.setOnClickListener { v  ->
            listener.onClickPost(postList[position])
        }


        holder.itemView.setOnLongClickListener { v  ->
            listener.onLongClickpost(postList[position])

            true
        }
    }

    override fun getItemCount(): Int {
        return postList.size
    }

    class PostViewHolder (inflater: LayoutInflater, parent: ViewGroup) :
        RecyclerView.ViewHolder(inflater.inflate(R.layout.item_post, parent, false)){

        private var tvTitle: TextView? = null

        init {
            tvTitle = itemView.findViewById(R.id.tvTitle)
        }

        fun bind(post: Post) {
            tvTitle?.text = post.title
        }

    }
}