package com.arango.miguel.sportafoliotest.ui.presenter

import com.arango.miguel.sportafoliotest.data.repository.PostRepositoryImpl
import com.arango.miguel.sportafoliotest.domain.callback.GetPostsCallback
import com.arango.miguel.sportafoliotest.domain.interactor.MainInteractor
import com.arango.miguel.sportafoliotest.domain.model.Post
import com.arango.miguel.sportafoliotest.domain.repository.PostRepository
import com.arango.miguel.sportafoliotest.ui.view.MainView

class MainPresenter: Presenter<MainView>(), GetPostsCallback {


    lateinit var interactor: MainInteractor

    override fun initialize() {
        interactor = MainInteractor(PostRepositoryImpl())

        getPostList()
    }

    fun getPostList(){
        interactor.getPosts(this)
    }

    override fun onGetPostsSuccess(postList: List<Post>) {
        getView()?.renderPosts(postList)
    }

    override fun onGetPostsError(errorMessage: String) {
        getView()?.showError(errorMessage)
    }

}