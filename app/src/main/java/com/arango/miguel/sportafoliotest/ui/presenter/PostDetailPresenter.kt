package com.arango.miguel.sportafoliotest.ui.presenter

import com.arango.miguel.sportafoliotest.data.repository.PostRepositoryImpl
import com.arango.miguel.sportafoliotest.domain.callback.GetPostDetailCallback
import com.arango.miguel.sportafoliotest.domain.interactor.PostDetailInteractor
import com.arango.miguel.sportafoliotest.domain.model.Post
import com.arango.miguel.sportafoliotest.ui.view.PostDetailView

class PostDetailPresenter(private val currentPost: Post, private val mode: String): Presenter<PostDetailView>(), GetPostDetailCallback {

    lateinit var interactor: PostDetailInteractor

    override fun initialize() {
        interactor = PostDetailInteractor(PostRepositoryImpl())

        if(mode.contentEquals("view")){
            getPostDetail(currentPost.id)
        }
    }

    fun getPostDetail(postId: Int){
        interactor.getPostDetail(postId, this)
    }

    override fun onGetPostDetailSuccess(post: Post) {
        getView()?.renderPost(post)
    }

    override fun onGetPostDetailError(errorMessage: String) {
        getView()?.showError(errorMessage)
    }

}