package com.arango.miguel.sportafoliotest.ui.view

interface BaseView {

    fun showError(errorMessage: String)
}