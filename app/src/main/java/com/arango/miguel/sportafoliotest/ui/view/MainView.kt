package com.arango.miguel.sportafoliotest.ui.view

import com.arango.miguel.sportafoliotest.domain.model.Post

interface MainView : BaseView {

    fun renderPosts(postList: List<Post>)
}