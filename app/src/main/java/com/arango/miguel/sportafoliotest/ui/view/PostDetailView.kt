package com.arango.miguel.sportafoliotest.ui.view

import com.arango.miguel.sportafoliotest.domain.model.Post

interface PostDetailView: BaseView {

    fun renderPost(postDetail: Post)
}